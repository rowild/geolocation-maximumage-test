import "./style.css"

document.addEventListener('DOMContentLoaded', e => {
  console.log('document loaded, e =', e);

  const timestampField = document.querySelector('.timestamp')
  const errorField = document.querySelector('.error')
  const maValue = document.querySelector('.ma-value')
  const latValue = document.querySelector('.lat')
  const lonValue = document.querySelector('.lon')


  const options = {
    enableHighAccuracy: true,
    maximumAge: 5000,
    timeout: 5000
  }

  maValue.textContent = options.maximumAge

  const checkPosition = pos => {
    console.log('pos =', pos);

    timestampField.textContent = pos.timestamp
    latValue.textContent = pos.coords.latitude
    lonValue.textContent = pos.coords.longitude
  }

  const positionError = err => {
    let errors = {
      1: 'No permission',
      2: 'Unable to determine',
      3: 'Took too long'
    }
    errorField.textContent = errors[err]
  }

  let watchId = undefined

  if (navigator.geolocation) {
    watchId = navigator.geolocation.watchPosition(
      checkPosition,
      positionError,
      options
    )
  }
  else {
    console.log('no geolocation');
  }

})